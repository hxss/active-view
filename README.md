# Active View
Applies custom settings to active view.

This plugin is reincarnation of [ActiveViewTheme](https://github.com/skuroda/ActiveViewTheme). But now you can customize not only `color_scheme` but any settings you want.

## Configuration

You can use menu `Preferences - Package Settings - ActiveView - Settings` or command palette command `Preferences: Active View settings` to open the settings.

In ActiveView settings file just put your configuration in `settings` object(will be created automatically).
