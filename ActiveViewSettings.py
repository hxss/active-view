import sublime
import sublime_plugin

import threading
import inspect
from pprint import pprint

class ActiveViewSettings(sublime_plugin.WindowCommand):
	TEMPLATE = "{\n\t\"settings\": {\n\t\t\n\t}\n}"
	POINT = (2, 2)

	def run(self):
		self.window.run_command('edit_settings', {"base_file": '${packages}/Default/Preferences.sublime-settings'})
		window = sublime.active_window()

		window.set_view_index(
			window.active_view(),
			0,
			len(window.views_in_group(0))
		)

		window.focus_group(1)
		view = window.open_file(sublime.packages_path() + "/User/ActiveView.sublime-settings")
		threading.Thread(target=self.template, args=(view,)).start()

	def template(self, view):
		while (view.is_loading()):
			pass

		if (view.size() == 0):
			view.run_command("append", {"characters": self.TEMPLATE})

			pt = view.text_point(self.POINT[0], self.POINT[1])
			view.sel().clear()
			view.sel().add(sublime.Region(pt))


