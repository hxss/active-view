import sublime
import sublime_plugin

class ActiveViewListener(sublime_plugin.EventListener):
	SETTINGS = 'ActiveView.sublime-settings'
	__settings = {}

	def __init__(self):
		self.settings_update()

		self.__settings.clear_on_change('settings')
		self.__settings.add_on_change(
			'settings',
			self.settings_update
		)

	def on_activated(self, view):
		if (not view.settings().get('is_widget')):
			for k, v in self.settings.items():
				view.settings().set(k, v)

	def on_deactivated(self, view):
		for k, v in self.settings.items():
			view.settings().erase(k)

	def settings_update(self):
		self.__settings = sublime \
			.load_settings(self.SETTINGS)

	@property
	def settings(self):
		settings = (self.__settings.get('settings') or {})

		if (not settings):
			self.settings_update()

		return settings
